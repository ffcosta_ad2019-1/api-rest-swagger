## Tutorial API publica documentanda com Swagger

# Resposta das questões do exercício
2.1) A API é de tempo real? Justifique  
R. Não é de tempo real, nem implementa websocket, necessario request para receber response. 

2.2) Documente a operação realizada utilizando Swagger ou AsyncAPI.  
Arquivo doc-swagger.yaml

2.3) Qual é a importância e vantagem da documentação das APIs?  
Facilita implementação da api 

Esta consulta de API foi criada com a API do portal da transparência de dispõe de documentos do governo federal de empenhos disponível em [portal da transparência](http://www.transparencia.gov.br/swagger-ui.html#!/Despesas32P250blicas/documentosUsingGET)  

Consulta qualquer documento do portal da transparência dos orgãos do governo federal.
# Passo a passo

1. Inicializando o projeto com package.json e as depêndencias iniciais
`npm init`

2. Acrescentar dependências no package.json e no script
```
    "express": "^4.17.0",
    "nodemon": "^1.19.1",
    "request": "^2.88.0"
```
3. Acrescentar dependências no package.json e no script
```
"start": "nodemon server.js"
```
 
4. Update package.json com `npm i`

7. Todo o codigo foi colocado no arquivo server.js o resultado da consulta é exibido no terminal
![server](img/server.png)  

8. São necessários os seguintes parâmetros na consulta da api, caso não sejam informados será preenchido pelos da Advocacia-Geral da União dia 14 fevereiro 2019:
- unidadeGestora
- gestao
- dataEmissao
- fase
- pagina

9. Teste realizado pelo postman
![Teste com params](img/postman1.png)  
![Teste sem params](img/postman2.png)  
